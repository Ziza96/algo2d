﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDR_Algo2D.Modele;


namespace DDR_Algo2D
{
    class Program
    {
        static void Main(string[] args)
        {

            Bande bande = new Bande(12);
            Caisse caisse121 = new Caisse(1, 12,20);
            LotdeCaisses lotDeCaisses = new LotdeCaisses(19, bande);
            lotDeCaisses.listDesCaisses.Add(caisse121);

            int i = 1;

            foreach (Caisse c in lotDeCaisses.listDesCaisses)
            {
                Console.WriteLine("Caisse "+i+" :"+c.longeur+" - "+c.largeur);
                i++;
            }

            do
            {
                Caisse caisse = lotDeCaisses.trouverCaisse(bande);
                Console.WriteLine(caisse.ToString());
                bande.PlaceCaise(caisse);
                lotDeCaisses.listDesCaisses.Remove(caisse);
                
                if(caisse.trouverPlusGrandeMesure() != 12)
                {
                    //Console.WriteLine(bande.trouverPlace());
                    //Console.WriteLine(caisse.nbCaisse);
                }
                //Console.ReadKey();
            } while (lotDeCaisses.listDesCaisses.Count > 0);


            bande.afficheBande();
            Console.ReadKey();
        }
    }
}
