﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDR_Algo2D.Modele
{
    class Bande
    {
        public int largeur { get; set ; }
        public int[,] bande { get;  set; }

        public Bande(int largeur)
        {
            this.largeur = largeur;
            //La longeur est à gerer autrement!
            this.bande = new int[240, largeur];

            for(int i = 0; i < 240; i++)
            {
                for(int j = 0; j<largeur; j++)
                {
                    bande[i, j] = 0;
                }
            }
        }

        public void afficheBande()
        {
            for (int i = 0; i < 240; i++)
            {
                
                for (int j = 0; j < largeur; j++)
                {
                    Console.Write(" | "+bande[i, j]);
                }
                Console.WriteLine(" | ");
            }
        }
       
        public int trouverPlace()
        {
            int compteurJ = 0;
            int compteurI = 0;
            for (int i = 0; i<240; i++)
            {
                if(bande [i,1] != 0)
                {
                    compteurI ++;
                } 
            }
            if(compteurI != 0)
            {
                for (int j = 0; j < largeur; j++)
                {
                    if (bande[compteurI - 1, j] == 0)
                    {
                        compteurJ++;
                    }
                }
            }
            else
            {
                compteurJ = 12;
            }
            
            return compteurJ;
        }

        public void PlaceCaise(Caisse caisseAPlacer)
        {
            int saveI = -1;
            int saveJ = -1;
            bool flagPlace = false;
            bool maj = true;
            int placeFlag = this.trouverPlace();

             for (int i = 0; i < 240; i++)
                {
                    for (int j = 0; j < largeur; j++)
                    {
                        if (bande[i, j] == 0 && i != saveI)
                        {
                            if (i + caisseAPlacer.trouverPlusPetiteMesure() <= 240)
                            {
                                for (int k = i; k <= i + caisseAPlacer.trouverPlusPetiteMesure() - 1; k++)
                                {
                                    if (j + caisseAPlacer.trouverPlusGrandeMesure() <= largeur)
                                    {
                                        for (int l = j; l < j + caisseAPlacer.trouverPlusGrandeMesure() - 1; l++)
                                        {
                                            if (bande[k, l] != 0)
                                            {
                                                flagPlace = false;
                                            }
                                            else
                                            {
                                                if (maj)
                                                {
                                                    saveI = i;
                                                    saveJ = j;
                                                    maj = false;
                                                    flagPlace = true;
                                                }

                                            }
                                        }
                                    }
                                }
                            }

                        }

                    }

                    if (flagPlace)
                    {
                        if (saveI + caisseAPlacer.trouverPlusPetiteMesure() <= 240)
                        {
                            for (int k = saveI; k < saveI + caisseAPlacer.trouverPlusPetiteMesure(); k++)
                            {
                                if (saveJ + caisseAPlacer.trouverPlusGrandeMesure() <= largeur)
                                {
                                    for (int l = saveJ; l < saveJ + caisseAPlacer.trouverPlusGrandeMesure(); l++)
                                    {
                                        bande[k, l] = caisseAPlacer.nbCaisse;
                                    }
                                }
                            }

                        }
                    }
                }
            
            
        } 
    }
}
