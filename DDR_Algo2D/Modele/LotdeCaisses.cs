﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDR_Algo2D.Modele;

namespace DDR_Algo2D.Modele
{
    class LotdeCaisses
    {
        public List<Caisse> listDesCaisses { get; set; }

        public LotdeCaisses(List<Caisse> listDesCaisses)
        {
            this.listDesCaisses = listDesCaisses;
        }

        public LotdeCaisses(int nbCaisses, Bande bande)
        {
            this.listDesCaisses = new List<Caisse>();

            Random randomMesure = new Random();

            for(int i = 0; i<nbCaisses; i++)
            {
                int largeur = randomMesure.Next(1, bande.largeur+1);
                int longeur = randomMesure.Next(1, bande.largeur+1);

                this.listDesCaisses.Add(new Caisse(largeur, longeur, i+1));
            }     
        }

        public Caisse trouverCaisse(Bande bande)
        {
            int maxMesure = 0;
            int minMesure = 0;
            int placeJ = bande.trouverPlace();

            List<Caisse> listCaisseAPlacer = new List<Caisse>();
            Caisse caisseAPlacer = new Caisse();

            //Ici, on cherche à trouver les caisses à placer  dans l'espace disponible
            for (int i = 0; i < this.listDesCaisses.Count(); i++)
            {
                if (listDesCaisses[i].trouverPlusGrandeMesure() == placeJ || listDesCaisses[i].trouverPlusPetiteMesure() == placeJ)
                {
                    listCaisseAPlacer.Add(listDesCaisses[i]);
                }
            }

            if(listCaisseAPlacer.Count() == 0)
            {
                //Ici, on cherche à trouver les caisses à placer avec le plus grand coté
                for (int i = 0; i < this.listDesCaisses.Count(); i++)
                {
                    if (listDesCaisses[i].trouverPlusGrandeMesure() >= maxMesure)
                    {
                        maxMesure = listDesCaisses[i].trouverPlusGrandeMesure();
                    }
                }
                for (int i = 0; i < this.listDesCaisses.Count(); i++)
                {
                    if (listDesCaisses[i].trouverPlusGrandeMesure() == maxMesure)
                    {
                        listCaisseAPlacer.Add(listDesCaisses[i]);
                    }
                }
            }
            
            //Ici on cherche la plus grands des plus petites valeur des caisses selectionnées.
            for (int i = 0; i<listCaisseAPlacer.Count(); i++)
            {
                if(listCaisseAPlacer[i].trouverPlusPetiteMesure() > minMesure)
                {
                    minMesure = listCaisseAPlacer[i].trouverPlusPetiteMesure();
                }
            }
            for (int i = 0; i < listCaisseAPlacer.Count(); i++)
            {
                if (listCaisseAPlacer[i].trouverPlusPetiteMesure() == minMesure)
                {
                   caisseAPlacer = listCaisseAPlacer[i];
                }
            }
            return caisseAPlacer;
        }
    }
}
