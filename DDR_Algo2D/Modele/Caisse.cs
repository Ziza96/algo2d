﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDR_Algo2D.Modele
{
    class Caisse
    {
        public int largeur { get;  set ; }
        public int longeur { get ; set; }
        public int volume { get; set; }
        public bool flagPlusGros { get; set; }
        public int nbCaisse { get; set; }

        public Caisse(int largeur, int longeur, int nbCaisse)
        {
            this.largeur = largeur;
            this.longeur = longeur;
            this.volume = longeur * largeur;
            this.nbCaisse = nbCaisse;
        }

        public Caisse()
        {
        }

        public int trouverPlusGrandeMesure()
        {
            if(longeur >= largeur)
            {
                return longeur;
            }
            else
            {
                return largeur;
            }
        }

        public int trouverPlusPetiteMesure()
        {
            if (longeur <= largeur)
            {
                return longeur;
            }
            else
            {
                return largeur;
            }
        }

        public override string ToString()
        {
            return "Caisse : "+longeur+" - "+largeur;
        }
    }
}
